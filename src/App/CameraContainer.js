import React, { useContext } from "react";
import { Redirect } from "react-router-dom";
import { AppContext } from "../AppContainer.js";

const Camera = () => {
  const app = useContext(AppContext);

  return app.isLoggedIn ? <div>AAAAAAAAAAA</div> : <Redirect to="/" />;
};

export default Camera;
