import React, { useState, useEffect, useContext } from "react";
import { BrowserRouter as Router, Route, NavLink } from "react-router-dom";
import axios from "axios";
import { AppContext } from "../AppContainer.js";
import HomeContainer from "../AppContainer.js";
import Back from "../Asset/back.svg";
import CouponBG from "../Asset/couponShape.png";
import Checklist from "../Asset/checklist.png";
import "./RedeemCouponContainer.css";

const BackHeader = () => {
  return (
    <React.Fragment>
      <div className="backRedeem">
        <NavLink to="/home" style={{ textDecoration: "none" }}>
          <img src={Back} alt="" />
        </NavLink>
      </div>
    </React.Fragment>
  );
};

const Coupon = props => {
  const [couponData, setCouponData] = useState({});
  const { couponClicked, couponId } = props;
  const app = useContext(AppContext);
  const { userId } = app.userData;

  const getCouponData = async () => {
    const { data } = await axios.get(
      `http://10.58.88.237:8888/coupon/getAllCouponDto/${userId}`
    );

    const choosenCoupon = data.find(coupon => coupon.couponId === couponId);
    setCouponData(choosenCoupon);
  };
  useEffect(() => {
    getCouponData();
  }, []);

  const postCouponData = () => {
    const couponRedeemed = {
      userId: userId,
      couponId: couponId
    };

    axios
      .post("http://10.58.88.237:8888/Trans/save-couponTrans", couponRedeemed)
      .then(res => {
        console.log(res.data);
      });
  };

  const sectionStyle = {
    Background: "cover",
    backgroundSize: "85vw 80vh",
    backgroundRepeat: "no-repeat",
    backgroundPosition: "center",
    backgroundImage: `url(${CouponBG})`
  };

  const btnClick = () => {
    couponClicked();
    postCouponData();
  };

  return (
    <div>
      <div className="couponContainer" style={sectionStyle}>
        <div className="couponDetail">
          <img
            className="couponImage"
            src={couponData.couponPic}
            alt={couponData.couponId}
          />
          <div className="couponName">{couponData.couponName}</div>
          <div className="couponDesc">{couponData.couponDesc}</div>
        </div>
        <div className="couponPrice">
          Coupon Price
          <div className="price">{couponData.couponPrice} pts</div>
        </div>

        <div className="redeemBtn" onClick={btnClick}>
          Redeem Now
        </div>
      </div>
    </div>
  );
};

const PopUp = props => {
  const { isPopupShown, okClicked } = props;
  return (
    <div
      className={isPopupShown ? "popUpContainerShow" : "popUpContainerClose"}
    >
      <div className="popUpShow">
        <div className="popUpImg">
          <img src={Checklist} alt="" />
        </div>
        <div className="popUpText">
          You have successfully redeemed this coupon.
        </div>
        <NavLink
          to="/home"
          style={{ textDecoration: "none", color: "#000000" }}
        >
          <div className="popUpBtn" onClick={okClicked}>
            OK
          </div>
        </NavLink>
      </div>
    </div>
  );
};

const RedeemContainer = match => {
  const [isCouponClicked, setIsCouponClicked] = useState(false);

  const openPopup = () => {
    setIsCouponClicked(true);
  };

  const closePopup = () => {
    setIsCouponClicked(false);
  };

  return (
    <React.Fragment>
      <Router>
        <Route path="/" exact component={HomeContainer} />
      </Router>

      <div className="couponPage">
        <div className="redeemContainer">
          <BackHeader />
          <Coupon
            couponClicked={openPopup}
            couponId={match.match.params.couponId}
          />
          <PopUp isPopupShown={isCouponClicked} okClicked={closePopup} />
        </div>
      </div>
    </React.Fragment>
  );
};

export default RedeemContainer;
