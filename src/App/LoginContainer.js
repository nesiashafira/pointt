import React, { useState, useContext } from "react";
import { Redirect } from "react-router-dom";
import axios from "axios";
import { AppContext } from "../AppContainer.js";
import Logo from "../Asset/HCID_logo.jpg";
import "./LoginContainer.css";

const LoginHeader = () => {
  return (
    <div className="headerLoginContainer">
      <div className="headerLogin">
        <img className="logoImg" src={Logo} alt="Home Credit" />
      </div>
    </div>
  );
};

// const PopupLogin = props => {
//   const { isPopupLoginShown } = props;

//   return (
//     <div className="popupLoginContainer">
//       <div className="popupLoginMsg">
//         Please enter your username and password.
//       </div>
//     </div>
//   );
// };

const Form = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const app = useContext(AppContext);

  const postLoginData = async () => {
    const LoginData = {
      userName: username,
      userpassword: password
    };

    try {
      const result = await axios.post(
        "http://10.58.92.195:8888/login",
        LoginData
      );

      const user = {
        userId: result.data.userId,
        username: result.data.userName,
        poin: result.data.userPoints
      };
      console.log(user);
      localStorage.setItem("userInfo", JSON.stringify(user));
      app.setIsLoggedIn(true);
    } catch (error) {
      setPassword("");
      setUsername("");
      // alert(error.response.data.message);
    }
  };

  const changeUsername = text => {
    setUsername(text.target.value);
    console.log(username);
  };

  const changePassword = text => {
    setPassword(text.target.value);
    console.log(password);
  };

  const handleLogin = () => {
    if (username.length === 0 || password.length === 0) {
      alert("kosong");
    } else {
      console.log("login");
      // app.setIsLoggedIn(true);
      postLoginData();
    }

    if (username === "coba" && password === "coba") {
      app.setIsLoggedIn(true);
    }
  };

  return app.isLoggedIn ? (
    <Redirect to="/home" />
  ) : (
    <div className="formContainer">
      <div className="formItem">
        <div className="formLabel">Username</div>
        <input
          type="text"
          className="inputText"
          name="username"
          value={username}
          onChange={changeUsername}
          onKeyUp={changeUsername}
        />
      </div>

      <div className="formItem">
        <div className="formLabel">Password</div>
        <input
          type="password"
          className="inputText"
          name="password"
          value={password}
          onChange={changePassword}
          onKeyUp={changePassword}
        />
      </div>
      <div className="formBtn" onClick={handleLogin}>
        Login
      </div>
    </div>
  );
};

const LoginContainer = () => {
  return (
    <div className="loginContainer">
      <LoginHeader />
      <Form />
    </div>
  );
};

export default LoginContainer;
