import React, { useState, useEffect, useContext } from "react";
import { NavLink, Redirect } from "react-router-dom";
import { AppContext } from "../AppContainer.js";
import axios from "axios";
import Header from "../Component/Header";
import Back from "../Asset/back.svg";
import redeemed from "../Asset/icon_redeemed.png";
import earned from "../Asset/icon_earned.png";
import "./HistoryContainer.css";

const BackHeader = () => {
  return (
    <React.Fragment>
      <div className="backHistory">
        <NavLink to="/home" style={{ textDecoration: "none" }}>
          <img src={Back} alt="" />
        </NavLink>
        <div className="pageHistory">History</div>
      </div>
    </React.Fragment>
  );
};

const TransactionList = props => {
  // const { day, date, icon, name, point } = props;
  const { transactionData } = props;

  return (
    <div>
      {transactionData &&
        transactionData.map(data => (
          <div key={data.transactionId} className="listContainer">
            <div className="transDate">
              <div className="date">{data.date}</div>
              <div className="day">{data.day.slice(0, 3)}</div>
            </div>
            <div className="nameContainer">
              <div
                className={
                  data.transType === "Coupon"
                    ? "transIconRedeemed"
                    : "transIconEarned"
                }
              >
                <img
                  src={data.transType === "Coupon" ? redeemed : earned}
                  alt=""
                />
              </div>
              <div className="transName">{data.titleName}</div>
              <div
                className={
                  data.transType === "Coupon"
                    ? "transPointRedeemed"
                    : "transPointEarned"
                }
              >
                {data.pointAffected}
              </div>
            </div>
          </div>
        ))}
    </div>
  );
};

const Transaction = () => {
  const [transData, setTransData] = useState([]);
  const app = useContext(AppContext);
  const { userId } = app.userData;

  const getTransData = async () => {
    const { data } = await axios.get(
      `http://10.58.88.237:8888/Trans/get-userHistory/${userId}`
    );
    await setTransData(data.histories.MAY);
  };

  useEffect(() => {
    getTransData();
  }, []);

  return (
    <div className="transaction">
      <div className="transContainer">
        <TransactionList transactionData={transData} />
      </div>
    </div>
  );
};

const HistoryContainer = () => {
  const app = useContext(AppContext);

  return app.isLoggedIn ? (
    <React.Fragment>
      <div className="historyContainer">
        <BackHeader />
        <Header />
        <Transaction />
      </div>
    </React.Fragment>
  ) : (
    <Redirect to="/" />
  );
};

export default HistoryContainer;
