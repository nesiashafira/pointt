import React, { useContext } from "react";
import {
  BrowserRouter as Router,
  Route,
  NavLink,
  Redirect
} from "react-router-dom";
import { AppContext } from "../AppContainer.js";
import Back from "../Asset/back.svg";
import HomeContainer from "../AppContainer.js";
import WheelContainer from "../Component/SpinningWheel.js";
import "./GameContainer.css";

const BackHeader = () => {
  return (
    <React.Fragment>
      <div className="backGame">
        <NavLink to="/home" style={{ textDecoration: "none" }}>
          <img src={Back} alt="" />
        </NavLink>
        <div className="pageGame">Wheel of Fortune</div>
      </div>
    </React.Fragment>
  );
};

const GameContainer = () => {
  const app = useContext(AppContext);

  return app.isLoggedIn ? (
    <React.Fragment>
      <Router>
        <Route path="/home" exact component={HomeContainer} />
      </Router>

      <div className="gameContainer">
        <BackHeader />
        <WheelContainer />
      </div>
    </React.Fragment>
  ) : (
    <Redirect to="/" />
  );
};

export default GameContainer;
