import React, { useState, useEffect, useContext } from "react";
import { NavLink, Redirect } from "react-router-dom";
import { AppContext } from "../AppContainer.js";
import axios from "axios";
import Slider from "react-slick";
import rouIcon from "../Asset/roulette.png";
import arIcon from "../Asset/ar.png";
import historyIcon from "../Asset/note.png";
import Header from "../Component/Header.js";
import "./HomeContainer.css";

const Navigation = () => {
  return (
    <React.Fragment>
      <div className="navContainer">
        <NavLink to="point-game" style={{ textDecoration: "none" }}>
          <div className="navItem">
            <div className=" navIcon">
              <img src={rouIcon} alt="" />
            </div>
            <div>Use Token</div>
          </div>
        </NavLink>

        <NavLink to="point-camera" style={{ textDecoration: "none" }}>
          <div className="navItem">
            <div className="navIcon">
              <img src={arIcon} alt="" />
            </div>
            <div>AR Camera</div>
          </div>
        </NavLink>

        <NavLink to="point-history" style={{ textDecoration: "none" }}>
          <div className="navItem">
            <div className="navIcon">
              <img src={historyIcon} alt="" />
            </div>
            <div>History</div>
          </div>
        </NavLink>
      </div>
    </React.Fragment>
  );
};

const PromotionItem = () => {
  const [coupon, setCoupon] = useState([]);
  const app = useContext(AppContext);
  // const { userId } = app.userData;

  const getCoupon = async () => {
    const { data } = await axios
      .get
      // `http://10.58.88.237:8888/coupon/getAllCouponDto/${userId}`
      ();
    await setCoupon(data);
  };

  useEffect(() => {
    getCoupon();
  }, []);

  const settings = {
    infinite: false,
    arrows: false,
    slidesToShow: 1.3,
    slidesToScroll: 1
  };

  return (
    <div className="promotionContainer">
      <div className="promotionTitle">Promotion</div>

      <Slider {...settings}>
        {coupon.map(data => (
          <NavLink
            key={data.couponId}
            to={`coupon/${data.couponId}`}
            style={{ textDecoration: "none" }}
          >
            <div className="promotionSlide">
              <div className="promotionList">
                <img
                  className="promotionListImage"
                  src={data.couponPic}
                  alt={data.couponId}
                />
                <div
                  className="promotionListPrice"
                  style={{ textDecoration: "none" }}
                >
                  {data.couponPrice} pts
                </div>
              </div>
            </div>
          </NavLink>
        ))}
      </Slider>
    </div>
  );
};

const HomeContainer = () => {
  const app = useContext(AppContext);

  return app.isLoggedIn ? (
    <div className="appContainer">
      <Header />
      <Navigation />
      <PromotionItem />
    </div>
  ) : (
    <Redirect to="/" />
  );
};

export default HomeContainer;
