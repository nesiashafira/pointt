import React from "react";
import PropTypes from "prop-types";
import "./SpinningWheel.css";

class SpinningWheel extends React.Component {
  componentDidMount() {
    this.updateCanvas();
  }

  updateCanvas() {
    function randPoint() {
      return Math.floor(Math.random() * 100 + 50);
    }

    const color = [
      "#FECF5D",
      "#02BD4C",
      "#00D3D7",
      "#3F5F92",
      "#D9445A",
      "#FF983B",
      "#498551",
      "#C33BFD"
    ];
    const label = [
      randPoint(),
      randPoint(),
      randPoint(),
      randPoint(),
      randPoint(),
      randPoint(),
      randPoint(),
      randPoint()
    ];
    const slices = color.length;
    const sliceDeg = 360 / slices;
    let deg = 360;
    let speed = 0;
    // let isSpin = false;
    // let lock = false;
    let slowdownRand = false;
    const ctx = this.refs.canvas.getContext("2d");
    const width = this.refs.canvas.width;
    const center = width / 2;

    this.setState = {
      isSpin: false,
      lock: false
    };

    const deg2rad = deg => {
      return (deg * Math.PI) / 180;
    };

    const drawSlice = (deg, color) => {
      ctx.beginPath();
      ctx.fillStyle = color;
      ctx.strokeStyle = "#ffffff";
      ctx.moveTo(center, center);
      ctx.arc(center, center, width / 2, deg2rad(deg), deg2rad(deg + sliceDeg));
      ctx.lineTo(center, center);
      ctx.fill();
    };

    const drawText = (deg, text) => {
      ctx.save();
      ctx.translate(center, center);
      ctx.rotate(deg2rad(deg));
      ctx.fillStyle = "#fff";
      ctx.font = "bold 30px sans-serif";
      ctx.fillText(text, 160, 10);
      ctx.restore();
    };

    const drawArrow = () => {
      ctx.beginPath();
      ctx.moveTo(210, 10);
      ctx.lineTo(280, 10);
      ctx.lineTo(246, 40);
      ctx.closePath();
      ctx.fillStyle = "#e11930";
      ctx.fill();
      ctx.strokeStyle = "white";
      ctx.stroke();
      ctx.lineWidth = 10;
    };

    const drawWheel = () => {
      ctx.clearRect(0, 0, center, center);

      for (var i = 0; i < slices; i++) {
        drawSlice(deg, color[i]);
        ctx.strokeStyle = "white";
        ctx.stroke();
        ctx.lineWidth = 10;

        drawText(deg + sliceDeg / 2, label[i]);
        deg += sliceDeg;

        drawArrow();
      }
    };

    const drawBtn = () => {
      ctx.beginPath();
      ctx.arc(center, center, 85, 0, 2 * Math.PI);
      ctx.lineWidth = 15;
      ctx.stroke();
      ctx.fillStyle = "white";
      ctx.fill();
      ctx.font = "bold 50px lato";
      ctx.fillStyle = "darkred";
      ctx.fillText("SPIN", center - 55, center + 20);
    };

    const drawSpinningWheel = () => {
      drawWheel();
      drawBtn();
    };

    function spin() {
      deg += speed;
      deg %= 360;

      if (this.isSpin) {
        speed = speed + 1 * 0.1;

        if (!this.lock) {
          this.lock = true;
          slowdownRand = Math.random() * (1.0 - 0.95) + 950;
        }
        speed = speed > 0.2 ? (speed *= slowdownRand) : 0;
      }

      if (this.lock && !speed) {
        var ai = Math.floor(((360 - deg - 90) % 360) / sliceDeg);
        ai = (slices + ai) % slices;
        return alert("You got:\n" + label[ai]);
      }
    }

    function getMousePos(canvas, event) {
      const rect = canvas.getBoundingClientRect();
      return {
        x: event.clientX - rect.left,
        y: event.clientY - rect.top
      };
    }

    function isInside(pos, rect) {
      return (
        pos.x > rect.x &&
        pos.x < rect.x + rect.width &&
        pos.y > rect.y &&
        pos.y < rect.y + rect.height
      );
    }

    function Clicked(evt) {
      const button = {
        x: 137,
        y: 116,
        width: 100,
        height: 100
      };
      const mousePos = getMousePos(this.refs.canvas, evt);

      if (isInside(mousePos, button)) {
        requestAnimationFrame(spin);
      } else {
        alert("clicked outside");
      }

      console.log(mousePos);
      console.log(button);
    }

    drawSpinningWheel();
  }

  // btnClick() {
  //   return (btnClicked = {});
  // }

  render() {
    return (
      <div className="wheel">
        <canvas
          className="canvas"
          ref="canvas"
          width={500}
          height={500}
          onClick={e => this.btnClick(e)}
        />
      </div>
    );
  }
}

SpinningWheel.propTypes = {
  btnClicked: PropTypes.func
};

const PointContainer = () => {
  return <div className="pointContainer" />;
};

const WheelToken = () => {
  return <div className="wheelTokenContainer">Token :</div>;
};

const WheelContainer = () => {
  return (
    <div className="wheelContainer">
      {/* <PointContainer /> */}
      <SpinningWheel />
      <WheelToken />
    </div>
  );
};

export default WheelContainer;
