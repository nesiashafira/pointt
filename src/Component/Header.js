import React, { useContext } from "react";
import { AppContext } from "../AppContainer.js";
import Background from "../Asset/backgroundMERAH.png";
import "./Header.css";

const Header = () => {
  const app = useContext(AppContext);
  const SectionStyle = {
    backgroundSize: "100% 100%",
    backgroundRepeat: "no-repeat",
    backgroundImage: `url(${Background})`
  };

  const PointDesc = () => {
    return (
      <div className="pointDesc">
        <div className="headTitle">Credit Point</div>
        <div className="headPoint">
          <div className="dot" />
          {/* <div className="point"> {app.userData.poin} pts</div> */}
        </div>
      </div>
    );
  };

  const Logout = () => {
    return (
      <div className="logout" onClick={handleLogout}>
        Log out
      </div>
    );
  };

  const handleLogout = () => {
    app.setIsLoggedIn(false);
    localStorage.clear();
  };

  return (
    <div className="headContainer" style={SectionStyle}>
      <Logout />
      <PointDesc />
    </div>
  );
};

export default Header;
