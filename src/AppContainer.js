import React, { useState } from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import HomeContainer from "./App/HomeContainer.js";
import LoginContainer from "./App/LoginContainer.js";
import HistoryContainer from "./App/HistoryContainer.js";
import GameContainer from "./App/GameContainer.js";
import Camera from "./App/CameraContainer.js";
import RedeemContainer from "./App/RedeemCouponContainer.js";
import "./AppContainer.css";

const AppContext = React.createContext();

let userData = localStorage.getItem("userInfo");
if (userData) userData = JSON.parse(userData);

const AppProvider = props => {
  const { children } = props;
  const [isLoggedIn, setIsLoggedIn] = useState(!!userData);
  const appValue = {
    isLoggedIn,
    setIsLoggedIn,
    userData
  };

  console.log(userData);

  return (
    <AppContext.Provider value={{ ...appValue }}>
      {children}
    </AppContext.Provider>
  );
};

const AppConsumer = AppContext.Consumer;
export { AppProvider, AppConsumer, AppContext };

const Container = () => {
  return (
    <AppProvider>
      <React.Fragment>
        <Router>
          <Route exact path="/" component={LoginContainer} />
          <Route path="/home" component={HomeContainer} />
          <Route path="/point-history" component={HistoryContainer} />
          <Route path="/point-game" component={GameContainer} />
          <Route path="/point-camera" component={Camera} />
          <Route path="/coupon/:couponId" component={RedeemContainer} />
        </Router>
      </React.Fragment>
    </AppProvider>
  );
};

export default Container;
